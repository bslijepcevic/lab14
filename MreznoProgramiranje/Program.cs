﻿using System;
using System.Net;

namespace MreznoProgramiranje {
    class Program {
        static void Main(string[] args) {
            var domena = Dns.GetHostEntry("vub.hr");
            Console.WriteLine(domena.HostName);
            foreach (var ip in domena.AddressList) {
                Console.WriteLine(ip);
            }
        }
    }
}
