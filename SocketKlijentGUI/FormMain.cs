using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketKlijentGUI
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void send()
        {
            string porukaString = tbMessage.Text;
            TcpClient klijent = new TcpClient(tbServerIP.Text, int.Parse(tbServerPort.Text));
            Byte[] poruka = Encoding.ASCII.GetBytes(porukaString);
            NetworkStream stream = klijent.GetStream();
            //Console.WriteLine(DateTime.Now + " - �aljem poruku!");
            stream.Write(poruka, 0, poruka.Length);
            stream.Close();
            //Console.WriteLine(DateTime.Now + " - poruka poslana!");
            klijent.Close();
        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            send();
        }
    }
}